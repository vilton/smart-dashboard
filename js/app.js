
var app = angular.module('app', []);

app.controller('mainController', function($scope, $timeout) {
	$scope.oi = 'text';
	$scope.sectionTab = 'login';

	$scope.likedLabels = []
	$scope.addInput = function(){
	  $scope.likedLabels.push({label:''});
	};
	$scope.removeInput = function(){
	  $scope.likedLabels.pop({label:''});
	};

	$timeout(function(){
		$(document).ready(function () {
		 //    $('.thumb a img').on('click', function(){
			// 	var href = $(this).attr('src');
			// 	$('.highlight img').attr('src', href) 
			// 	return false
			// });

			$('.progress-circle').circleProgress({
				size: 175,
				startAngle: 55,
				thickness: 30,
				emptyFill: '#e8e8f0',
				fill: {
				gradient: ["#42a4a3"]
				}
			});

			$('.progress-bar').gradientProgressBar({
				size: 598,
				thickness: 20,
				emptyFill: 'transparent',
				fill: {
				gradient: ["#42a4a3"]
				}
			});



			$(document).on('change', '.buildSelect', function (e) {
				e.preventDefault();
				e.stopImmediatePropagation();

				var categpai = $(this).val();

				$.ajax({
					type: "GET",
					url: "controllers/select-rooms.php",
					data: "pai="+categpai,
					success: function(data){
						$('.roomList option').remove();
						$('.roomList').append(data);
						// if($( ".categ" ).hasClass( "selectpicker" )) {
						// 	$('.catego .bootstrap-select').remove();
						// 	$('.categ').remove();
						// 	$(".catego").wrap("<div class=\"catego\"><select class=\"selectpicker categ\" name=\"idcategoria\" /></div>");
						// 	$('.selectpicker.categ').append(data);
						// 	$('.selectpicker.categ').selectpicker();
						// } else {
						// 	$('.categ').addClass('selectpicker');
						// 	$('.categ option').remove();
						// 	$('.selectpicker.categ').append(data);
						// 	$('.selectpicker.categ').selectpicker();
						// }
					}
				});
			});
		});
	});
});

window.onload = function() {
    // Intensify all images on the page.
    var elements = document.querySelectorAll('.intense');
	Intense(elements);
}

$(document).ready(function(){
	$("#calendar-images .carousel").owlCarousel({
		autoPlay	 : false,
		items: 1,
		navigation : false, // Show next and prev buttons
		pagination : false,
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true,
		addClassActive: true,
		beforeInit : function (elem) {
			$('.thumbs-container .image-box').eq(0).find('a').addClass('selected');
		},
		afterMove : function (elem) {
			var current = this.currentItem;
			$('.thumbs-container .image-box').parents().find('a').removeClass('selected');
			$('.thumbs-container .image-box').eq(current).find('a').addClass('selected');
		}
	});


	var owlContainer = $("#calendar-images .carousel");
	owlContainer.owlCarousel();

	$(".thumbs-container .image-box").click(function(){
		// owl.goTo($(this).index());
		owlContainer.trigger('to.owl.carousel', $(this).index())
		$(this).parents().find('a').removeClass('selected');
		$(this).find('a').addClass('selected');
		// owl.trigger('owl.currentItem[1]');
		return false;
	})
	$(".nextpic").click(function(){
		owlContainer.trigger('next.owl.carousel');
	})
	$(".prevpic").click(function(){
		owlContainer.trigger('prev.owl.carousel');
	})

	$("#calendar-images .months li a").on('click', function(){
		$('#calendar-images .months li a').removeClass('active');
		$(this).addClass('active');
		// return false
	})

	$('#slide-images .carousel').owlCarousel({
		items: 1,
		nav: true,
		dots: true
	});

	$('#calendar-images').removeClass('hidden');
});