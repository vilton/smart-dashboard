<?php
	require('autoload.php');
	session_name('user');
	session_start();

	if(isset($_SESSION['LOGIN']['ID']) && $_SESSION['LOGIN']['MANAGER'] != 1){
		$iduser = (int) $_SESSION['LOGIN']['ID'];

		if(!empty($_GET['room'])) {
			$idroom = (int) $_GET['room'];

			$room = new Room();
			$room->Carrega($idroom);

			$idbuild = $room->getBuild_idbuild();
		} else {
			$query_root = "SELECT Room_idroom 
				 FROM User_has_Room
				 WHERE User_iduser = {$iduser}
				 LIMIT 1";

			$db_root= new DB();
			$db_root->Sql($query_root);

			$dado_root= $db_root->Fetch();

			$idroom = $dado_root->Room_idroom;

			$room = new Room();
			$room->Carrega($idroom);

			$idbuild = $room->getBuild_idbuild();
		}

		$query_menu = "SELECT Room_idroom 
				 FROM User_has_Room
				 WHERE User_iduser = {$iduser}";

		$db_menu= new DB();
		$db_menu->Sql($query_menu);

		$build = new Build();
		$build->Carrega($idbuild);

		// if(!empty($idroom)) {
		// 	$idroom = $build->get
		// }
	}
?>
<!DOCTYPE html>
<html ng-app="app">
<head>
<link rel="stylesheet" href="stylesheets/assets/owl.carousel.css">


<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" href="stylesheets/main.css">

  <meta charset="utf-8">
  <title>Smart</title>
</head>
<body ng-controller="mainController">
	<!-- <div ng-include="'views/components/header.html'"></div> -->

	<?php
	    if(!isset($_SESSION['LOGIN']['ID'])){
	    	include("views/login.html");
	    } else if(isset($_GET['t'])) {
	    	if($_SESSION['LOGIN']['MANAGER'] == 1) {
		    	include("views/components/header-admin.php");
		    } else {
		    	include("views/components/header.php");
		    }
	    	include("views/{$_GET['t']}.php");
		} else {
	    	if($_SESSION['LOGIN']['MANAGER'] == 1) {
		    	include("views/components/header-admin.php");
				include("views/builds.php");
		    } else {
		    	include("views/components/header.php");
		    	$build_index = $build->getIdbuild();
				include("views/statistics.php");
		    }
		}
	?>
	<script src="js/libs/angular.min.js"></script>
	<script src="js/libs/jquery-2.1.4.min.js"></script>
	<script src="js/libs/owl.carousel.min.js"></script>
	<script src="js/libs/gradient-progress-bar.js"></script>
	<script src="js/libs/circle-progress.js"></script>
	<script src="js/libs/intense.min.js"></script>

	<script type="text/javascript" src="js/app.js"></script>
</body>
</html>