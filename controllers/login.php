<?php
	require('../autoload.php');
	try{
		$db= new DB();

		$email = Util::LimpaStringCompleta($_POST['email']);
		$cpf = Util::LimpaStringCompleta($_POST['cpf']);

		$query= "SELECT * 
				 FROM User
				 WHERE cpf = '{$cpf}' AND
					   email = '{$email}'";

		if(!$db->Sql($query)){
			throw new Exception('Erro ao verificar o usu�rio');
		}

		if($db->NumRows() != 1){
			throw new Exception('Usu�rio ou senha inv�lidos');
		}

		$dado= $db->Fetch();

		session_name('user');
		session_start();
		$_SESSION['LOGIN']['ID']= $dado->iduser;
		$_SESSION['LOGIN']['CPF']= $dado->cpf;
		$_SESSION['LOGIN']['EMAIL']= $dado->email;
		$_SESSION['LOGIN']['MANAGER']= $dado->manager;
		$_SESSION['LOGIN']['DATE']= date('Y-m-d H:i:s');

		header('Location: ../index.php');
	}catch(Exception $e){
		Util::Mensagem($e->getMessage());
		Util::Redireciona('../index.php');
	}
?>