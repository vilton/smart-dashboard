<?php
    session_name('user');
    session_start();
    
    //mata a variavel $_SESSION
    session_unset();
    
    //mata a sess�o
    session_destroy();
    
    header('Location: ../index.php');
?>