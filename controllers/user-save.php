<?php
	require('../autoload.php');

	try{
		//cria uma instância da classe Cliente
		$user = new User();

		//alimenta a classe utilizando os métodos "set"
		$user->setEmail($_POST['email']);
		$user->setCpf($_POST['cpf']);
		$user->setIdroom($_POST['room']);

		//cadastra no banco de dados

		if($_POST['edit'] == 1) {
			$user->setIduser($_POST['user']);
			$user->Edita();
		} else {
			$user->Cadastra();
			$user->CadastraSala();
		}

		Util::Mensagem("Cadastrado com sucesso");
		$url = "../index.php?t=users";
		Util::Redireciona($url);
	}catch(Exception $e){
		Util::Mensagem($e->getMessage());
		Util::Redireciona('javascript: history.go(-1)');
	}
?>