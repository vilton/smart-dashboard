<?php
	require('../autoload.php');

	try{
		//cria uma inst�ncia da classe Cliente
		$build = new Build();

		//alimenta a classe utilizando os m�todos "set"
		$build->setName($_POST['name']);
		$build->setDescription($_POST['description']);
		$build->setStats($_POST['stats']);

		//cadastra no banco de dados
		if($_POST['type'] == 1) {
			$build->setIdbuild($_POST['build']);
			$build->Edita();
		} else {
			$build->Cadastra();
		}

		Util::Mensagem("Cadastrado com sucesso");
		$url = "../index.php?t=room-new&build={$build->getIdbuild()}";
		Util::Redireciona($url);
	}catch(Exception $e){
		Util::Mensagem($e->getMessage());
		Util::Redireciona('javascript: history.go(-1)');
	}
?>