<?php
	require('../autoload.php');

	try{
		//cria uma instância da classe Cliente
		$user = new User();

		//alimenta a classe utilizando os métodos "set"
		$user->setIduser($_POST['user']);
		$user->setIdroom($_POST['room']);

		//cadastra no banco de dados
		$user->CadastraSala();

		Util::Mensagem("Cadastrado com sucesso");
		$url = "../index.php?t=users";
		Util::Redireciona($url);
	}catch(Exception $e){
		Util::Mensagem($e->getMessage());
		Util::Redireciona('javascript: history.go(-1)');
	}
?>