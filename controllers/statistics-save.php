<?php
	require('../autoload.php');

	try{
		$progress = new Progress();

		$month = $_POST['month'];
		$year = $_POST['year'];
		$date = $year . '-' . $month . '-01';

		$progress->setDate($date);
		$progress->setBuild_idbuild($_POST['idbuild']);
		$progress->setProjects($_POST['projects']);
		$progress->setGround($_POST['ground']);
		$progress->setFoundations($_POST['foundations']);
		$progress->setExcavation($_POST['excavation']);
		$progress->setStructures($_POST['structures']);
		$progress->setMasonry($_POST['masonry']);
		$progress->setFrames($_POST['frames']);
		$progress->setWaterproofing($_POST['waterproofing']);
		$progress->setCoatings($_POST['coatings']);
		$progress->setpaving($_POST['paving']);
		$progress->setElectrical($_POST['electrical']);
		$progress->setLandscaping($_POST['landscaping']);
		$progress->setDecoration($_POST['decoration']);

		$progress->Cadastra();

		$url = "../index.php?t=builds";

		Util::Mensagem("Cadastrado com sucesso");
		Util::Redireciona($url);
	}catch(Exception $e){
		Util::Mensagem($e->getMessage());
		Util::Redireciona('javascript: history.go(-1)');
	}
?>