<?php
    //error_reporting(0);//oculta TODOS os erros do PHP
    require('autoload.php');
    session_name('user');
    session_start();
    if(!isset($_SESSION['LOGIN']['ID'])){
        Util::Mensagem('Acesso indevido');
        Util::Redireciona('index.php');
        exit();
    }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Gerenciador</title>
	
    <link rel="stylesheet" type="text/css" href="style/reset.css" /> 
    <link rel="stylesheet" type="text/css" href="style/root.css" /> 
    <link rel="stylesheet" type="text/css" href="style/grid.css" /> 
    <link rel="stylesheet" type="text/css" href="style/typography.css" /> 
    <link rel="stylesheet" type="text/css" href="style/jquery-ui.css" />
    <link rel="stylesheet" type="text/css" href="style/jquery-plugin-base.css" />
    
    <!--[if IE 7]>	  <link rel="stylesheet" type="text/css" href="style/ie7-style.css" />	<![endif]-->
    
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/adicionarcampo.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.11.custom.min.js"></script>
	<script type="text/javascript" src="js/jquery-settings.js"></script>
	<script type="text/javascript" src="js/toogle.js"></script>
	<script type="text/javascript" src="js/jquery.tipsy.js"></script>
	<script type="text/javascript" src="js/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
	<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
	<script type="text/javascript" src="js/raphael.js"></script>
	<script type="text/javascript" src="js/analytics.js"></script>
	<script type="text/javascript" src="js/popup.js"></script>
	<script type="text/javascript" src="js/fullcalendar.min.js"></script>
	<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
	<script type="text/javascript" src="js/jquery.ui.core.js"></script>
	<script type="text/javascript" src="js/jquery.ui.mouse.js"></script>
	<script type="text/javascript" src="js/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="js/jquery.ui.slider.js"></script>
	<script type="text/javascript" src="js/jquery.ui.datepicker.js"></script>
	<script type="text/javascript" src="js/jquery.ui.tabs.js"></script>
	<script type="text/javascript" src="js/jquery.ui.accordion.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="js/funcoes.js"></script>

      <style>#stats{display:block;}</style>          
		<script type="text/javascript">
			$(document).ready(function(){
				$("area[rel^='prettyPhoto']").prettyPhoto();
				
				$(".get-photo a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square',slideshow:3000, autoplay_slideshow: true});
				$(".get-photo a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000, hideflash: true});
		
				$("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
					custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
					changepicturecallback: function(){ initialize(); }
				});

				$("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
					custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
					changepicturecallback: function(){ _bsap.exec(); }
				});
			});
		</script>
</head>
<body>

<div class="wrapper">
	<!-- START HEADER -->
    <div id="header">
    	<!-- logo -->
    	<div class="logo">
        	<a href="principal.php">
            	<img src="img/logo.png" width="112" height="35" alt="logo"/>
            </a>
        </div>      
        <!-- profile box -->
        <div id="profilebox">
        	<a href="#" class="display">
            	<img src="img/simple-profile-img.jpg" width="33" height="33" alt="profile"/>	<b>Logado como</b>	<span><?php echo $_SESSION['LOGIN']['NOME'] ?></span>
            </a>
            
            <div class="profilemenu">
            	<ul>
                	<li><a href="logoff.php">Sair</a></li>
                </ul>
            </div>
            
        </div>
        <div class="clear"></div>
    </div>
    <!-- END HEADER -->
    <!-- START MAIN -->
    <div id="main">
        <!-- START SIDEBAR -->
        <div id="sidebar">
        	
            <!-- start searchbox -->
            <div id="searchbox">

            </div>
            <!-- end searchbox -->
            
            <!-- start sidemenu -->
            <div id="sidemenu">
            	<ul>
                    <li <?php if($_GET['t'] == "secao" || $_GET['t'] == "novasecao" || $_GET['t'] == "editasecao") { echo "class='active'"; } else { } ?>>
                        <a href="principal.php?page=secoes&t=secao"><img src="img/icons/sidemenu/copy.png" width="16" height="16" alt="icon"/>Seções</a>
                    </li>
                    <li <?php if($_GET['t'] == "conteudo" || $_GET['t'] == "novoconteudo" || $_GET['t'] == "editaconteudo") { echo "class='active'"; } else { } ?>>
                        <a href="principal.php?page=conteudo&t=conteudo"><img src="img/icons/sidemenu/attach.png" width="16" height="16" alt="icon"/>Conteúdo</a>
                    </li>
                    <li <?php if($_GET['t'] == "categorias" || $_GET['t'] == "novacategoria" || $_GET['t'] == "editacategoria") { echo "class='active'"; } else { } ?>>
                        <a href="principal.php?page=categorias&t=categorias"><img src="img/icons/sidemenu/copy.png" width="16" height="16" alt="icon"/>Categorias</a>
                    </li>
                    <li <?php if($_GET['t'] == "tipos" || $_GET['t'] == "novotipo" || $_GET['t'] == "editatipo") { echo "class='active'"; } else { } ?>>
                        <a href="principal.php?page=tipos&t=tipos"><img src="img/icons/sidemenu/file_edit.png" width="16" height="16" alt="icon"/>Tipos</a>
                    </li>
                    <li <?php if($_GET['t'] == "zonas" || $_GET['t'] == "novazona" || $_GET['t'] == "editazona") { echo "class='active'"; } else { } ?>>
                        <a href="principal.php?page=zonas&t=zonas"><img src="img/icons/sidemenu/attach.png" width="16" height="16" alt="icon"/>Zonas</a>
                    </li>
                    <li <?php if($_GET['t'] == "clientes" || $_GET['t'] == "novocliente" || $_GET['t'] == "editacliente") { echo "class='active'"; } else { } ?>>
                        <a href="principal.php?page=clientes&t=clientes&tipo=1"><img src="img/icons/sidemenu/user.png" width="16" height="16" alt="icon"/>Clientes</a>
                    </li>
                    <li <?php if($_GET['t'] == "imobiliaria" || $_GET['t'] == "novaimobiliaria" || $_GET['t'] == "editaimobiliariar") { echo "class='active'"; } else { } ?>>
                        <a href="principal.php?page=clientes&t=clientes&tipo=2"><img src="img/icons/sidemenu/calendar.png" width="16" height="16" alt="icon"/>Imobiliaria</a>
                    </li>
                    <li <?php if($_GET['t'] == "badges" || $_GET['t'] == "novobadge" || $_GET['t'] == "editabadge") { echo "class='active'"; } else { } ?>>
                        <a href="principal.php?page=badges&t=badges"><img src="img/icons/sidemenu/star.png" width="16" height="16" alt="icon"/>Badges</a>
                    </li>
                    <!-- start submenu with icon -->
                    <li class="subtitle tips-right">
                    	<a class="action tips-right" href="principal.php?page=imoveis&t=imoveis" title="Submenu de imoveis">
                            <img src="img/icons/sidemenu/mail.png" width="16" height="16" alt="icon"/>
                            Imóveis
                            <img src="img/arrow-down.png" width="7" height="4" alt="arrow" class="arrow" />
                        </a>
                    	<ul class="submenu" 
                        <?php if($_GET['t'] == "novoimovel" || $_GET['t'] == "imoveis" ||
                                 $_GET['t'] == "editaimovel" || $_GET['t'] == "tiporeferencia" || 
                                 $_GET['t'] == "editatiporeferencia" || $_GET['t'] == "novotiporeferencia" || $_GET['t'] == "imoveislanc") { 
                            echo "style='display:block'"; } 
                        ?>>
							<li <?php if($_GET['t'] == "novoimovel") { echo "class='active'"; } ?>>
                                <a href="principal.php?page=imoveis&t=novoimovel">
                                    <img src="img/icons/sidemenu/file.png" width="16" height="16" alt="icon"/>
                                    Adicionar
                                </a>
                            </li>
                            <li <?php if($_GET['t'] == "imoveis" || $_GET['t'] == "editaimovel") { echo "class='active'"; } ?>>
                                <a href="principal.php?page=imoveis&t=imoveis">
                                    <img src="img/icons/sidemenu/file_edit.png" width="16" height="16" alt="icon"/>
                                    Editar
                                </a>
                            </li>
                       </ul>
                    </li>
                    <!-- end submenu with icon -->
                    
                </ul>
            </div>
            <!-- end sidemenu -->
            
        </div>
        <!-- END SIDEBAR -->

        
        <!-- START PAGE -->
        <div id="page">            	
                
                <!-- start page title -->
                <div class="page-title">
                	<div class="in">
                    	<div class="titlebar">
                           	<h2>GERENCIADOR</h2>
                            <p>Selecione no ao lado o conteúdo que você deseja editar.</p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <!-- end page title -->
                	<!-- START CONTENT -->
                    <div class="content">
					<?php
						//inclui as telas
						if(isset($_GET['t'])){
							include("telas/{$_GET['page']}/sist_{$_GET['t']}.php");
						}
						else {
							include("telas/sist_inicio.php");
						}
					?>
                    </div>
                    <!-- END CONTENT -->
        </div>
        <!-- END PAGE -->
    <div class="clear"></div>
    </div>
    <!-- END MAIN -->
</div>
</body>
</html>