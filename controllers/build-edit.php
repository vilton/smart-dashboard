<?php
    try{
        //cria uma instância da classe Cliente
        $imovel= new Imovel();

        //alimenta a classe utilizando os métodos "set"
	    $imovel->setIdimovel($_POST['codigo']);
        $imovel->setIdcategoria($_POST['categoria']);
        $imovel->setTipo($_POST['tipo']);
        $imovel->setIdzona($_POST['zona']);
        $imovel->setIdcliente($_POST['cliente']);
        $imovel->setIdimobiliaria($_POST['imobiliaria']);
        $imovel->setTitulo($_POST['titulo']);
        $imovel->setDescricao($_POST['descricao']);
        $imovel->setEndereco($_POST['endereco']);
        $imovel->setComplemento($_POST['complemento']);
        $imovel->setBairro($_POST['bairro']);
        $imovel->setCep($_POST['cep']);
        $imovel->setCidade($_POST['cidade']);
        $imovel->setEstado($_POST['estado']);
        $imovel->setPais($_POST['pais']);
        $imovel->setQuartos($_POST['quartos']);
        $imovel->setAreaprivativa($_POST['areaprivativa']);
        $imovel->setAreatotal($_POST['areatotal']);
        $imovel->setGaragem($_POST['garagem']);
        $imovel->setSuite($_POST['suite']);
        $imovel->setIptu($_POST['iptu']);
        $imovel->setElevador($_POST['elevador']);
        $imovel->setObservacoes($_POST['observacoes']);
        $imovel->setPreco($_POST['preco']);
        $imovel->setAluguel($_POST['aluguel']);
        $imovel->setCondominio($_POST['condominio']);
        $imovel->setComissao($_POST['comissao']);
        $imovel->setCondpgto($_POST['condicoespagamento']);
        $imovel->setFinanciamento($_POST['financiamento']);
        $imovel->setBanco($_POST['banco']);
        $imovel->setDivida($_POST['divida']);
        $imovel->setProprietario($_POST['proprietario']);
        $imovel->setRg($_POST['rg']);
        $imovel->setCpf($_POST['cpf']);
        $imovel->setInfo($_POST['info']);
        $imovel->setContato($_POST['contato']);
        $imovel->setTemporada($_POST['temporada']);
        $imovel->setIniciotemporada($imovel->formataData($_POST['iniciotemporada']));
        $imovel->setFimtemporada($imovel->formataData($_POST['fimtemporada']));

        //cadastra no banco de dados
        $imovel->Edita();
        $imovel->CadastraFoto();

	    Util::Mensagem("{$imovel->getTitulo()} editado com sucesso");
        Util::Redireciona('principal.php?page=imoveis&t=imoveis');

    }catch(Exception $e){
        Util::Mensagem($e->getMessage());
        Util::Redireciona('javascript: history.go(-1)');
    }
?>