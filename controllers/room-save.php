<?php
	require('../autoload.php');

	try{
		for($i = 0; $i < count($_POST['room']); $i++) {
			if(!empty($_POST['room'][$i])) {
				$room = new Room();

				$ref = substr($_POST['buildname'], 0, 3); // retorna "d"
				$ref_final = $ref . $_POST['room'][$i];

				$room->setName($_POST['room'][$i]);
				$room->setReference($ref_final);
				$room->setBuild_idbuild($_POST['idbuild'][$i]);

				$room->Cadastra();
			}
		}

		Util::Mensagem("Cadastrado com sucesso");
		$url = "../index.php?t=photos-new&build={$room->getBuild_idbuild()}";
		Util::Redireciona($url);
	}catch(Exception $e){
		Util::Mensagem($e->getMessage());
		Util::Redireciona('javascript: history.go(-1)');
	}
?>