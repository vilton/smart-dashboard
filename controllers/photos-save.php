<?php
	require('../autoload.php');

	try{
		$file = $_FILES['photo'];

		$month = $_POST['month'];
		$year = $_POST['year'];
		$date = $year . '-' . $month . '-01';
		// echo $date;
		// echo $newdate;
		// $dateformat = new DateTime($newdate);
		// $date = date_format($dateformat, 'Y-m-d');

		if(count($file['name'] <= 0)) {
			throw new Exception('Preencha os campos corretamente');
		}

		for($i = 0; $i < count($file['name']); $i++) {
			if(!empty($file['name'][$i])) {
				$photo = new Photo();

				$photo->setDate($date);
				$photo->setTitle($_POST['title'][$i]);
				$photo->setDescription($_POST['description'][$i]);
				$photo->setFile($file["name"][$i]);
				$photo->setFileTemp($file["tmp_name"][$i]);
				$photo->setType($_POST['type']);
				$photo->setBuild_idbuild($_POST['idbuild']);

				$photo->cadastraFoto();
			}
		}

		// Util::Mensagem("Fotos cadastradas com sucesso");
		if($photo->getType() == 1) {
			$url = "../index.php?t=success-message";
		} else {
			$url = "../index.php?t=photos-new&build={$photo->getBuild_idbuild()}&type=1";
		}
		Util::Redireciona($url);
	} catch(Exception $e){
		Util::Mensagem($e->getMessage());
		Util::Redireciona('javascript: history.go(-1)');
	}
?>