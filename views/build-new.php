<?php 
	if(!empty($_GET['build'])) {
		$idbuild = (int) $_GET['build'];

		$build = new Build();
		$build->Carrega($idbuild);
	}
?>
<div id="build-new" class="section">
	<div class="build-box">
		<h1 class="title">
			<?php 
				if(!empty($_GET['build'])) { 
					echo "Editar Empreendimento"; 
				} else { 
					echo "Cadastrar Empreendimento"; 
				} 
			?>
		</h1>
		<form action="controllers/build-save.php" method="POST">
			<input type="hidden" name="build" value="<?php if(!empty($_GET['build'])) { echo $build->getIdbuild(); } ?>">
			<input type="hidden" name="type" value="<?php if(!empty($_GET['build'])) { echo "1"; } else { echo "0"; } ?>">
			<input type="text" name="name" required placeholder="Nome empreendimento" value="<?php if(!empty($_GET['build'])) { echo $build->getName(); } ?>">
			<input type="text" name="stats" placeholder="Status geral" value="<?php if(!empty($_GET['build'])) { echo $build->getStats(); } ?>">
			<textarea class="description" name="description" placeholder="Observações gerais.."><?php if(!empty($_GET['build'])) { echo $build->getDescription(); } ?></textarea>
			<button class="enter" ng-click="sectionTab = 'calendar'">Salvar</button>
		</form>
	</div>
</div>