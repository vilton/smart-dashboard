<?php
	$iduser = (int) $_GET['user'];

	$query= "SELECT idbuild 
			 FROM Build";
	$db= new DB();
	$db->Sql($query);

	$user = new User();
	$user->Carrega($iduser);
?>
<div id="user-new">
	<div class="user-new-content">
		<h1 class="title"><?php echo $user->getEmail() ?></h1>
		<h2 class="subtitle">Cadastro de Sala</h1>
		<div>
			<form action="controllers/user-room-save.php" method="POST">
				<input type="hidden" name="user" value="<?php echo $iduser ?>">
				<label>
					<select name="build" class="buildSelect">
						<option value="">Selecione um empreendimento</option>";
						<?php
							while($dado= $db->Fetch()){
								$build = new Build();
								$build->Carrega($dado->idbuild);
								echo "
							<option value=\"{$build->getIdbuild()}\">{$build->getName()}</option>";
							}
						?>
					</select>
				</label>
				<label>
					<select name="room" class="roomList">
						<option value="">Selecione uma sala</option>
						
					</select>
				</label>
				<button class="enter">OK</button>
			</form>
		</div>
	</div>
</div>