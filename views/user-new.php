<?php 
	$query= "SELECT idbuild 
			 FROM Build";
	$db= new DB();
	$db->Sql($query);

	$query2= "SELECT idroom 
			 FROM Room";
	$db2= new DB();
	$db2->Sql($query2);
?>
<div id="user-new">
	<div class="user-new-content">
		<h1 class="title">Cadastro de usuários</h1>
		<div>
			<form action="controllers/user-save.php" method="POST">
				<label>
					<span>CPF</span>
					<input type="text" name="cpf" required>
				</label>
				<label>
					<span>E-mail</span>
					<input type="text" name="email" required>
				</label>
				<label>
					<select name="build" class="buildSelect" required>
						<option value="">Selecione um empreendimento</option>";
						<?php
							while($dado= $db->Fetch()){
								$build = new Build();
								$build->Carrega($dado->idbuild);
								echo "
							<option value=\"{$build->getIdbuild()}\">{$build->getName()}</option>";
							}
						?>
					</select>
				</label>
				<label>
					<select name="room" class="roomList" required>
						<option value="">Selecione uma sala</option>
						
					</select>
				</label>
				<button class="enter">OK</button>
			</form>
		</div>
	</div>
</div>