<?php 
	// $idbuild = (int) $_GET['build'];
	$type = (int) $_GET['type'];

	$datePost = $_GET['date'];
	$photo = new Photo();

	// if(empty($datePost)) {
	// 	$photo->loadLast($idbuild);
	// } else {
	// 	$photo->Carrega($datePost);
	// }

	// $build = new Build();
	// $build->Carrega($idbuild);

	$query= "SELECT 
					DISTINCT(date)
			 FROM 
			 	Photo
			 WHERE 
			 	Build_idbuild = '$idbuild'
			 AND
			 	type = $type
			 ORDER BY
			 	date
			 DESC";
	$db= new DB();
	$db->Sql($query);

	if($datePost == "") {
		$db2= new DB();
		$db2->Sql($query);

		$dadoFirsth= $db2->Fetch();
		$month = date("m",strtotime($dadoFirsth->date));
	} else {
		$month = date("m",strtotime($datePost));
	}

	$queryPhotos="SELECT 
					*
				FROM 
			 		Photo
			 	WHERE
			 		Build_idbuild = '$idbuild'
			 	AND
			 		type = $type
			 	AND 
			 		MONTH(date) = $month
			 	ORDER BY
			 		date
			 	DESC
			 	LIMIT
			 		4";
	$dbp= new DB();
	$dbp->Sql($queryPhotos);

	$dbp2= new DB();
	$dbp2->Sql($queryPhotos);
?>
<div id="calendar-images" class="section hidden" ng-class="{'show':sectionTab==='calendar'}">
	<div class="content">
		<div class="title-pdf">
			<h1 class="title"><?php echo $build->getName() ?></h1>
			<a class="download-pdf button" style="display: none">Download PDF</a>
		</div>
		<div class="wrapper">
			<div class="top-content">
				<ul class="months">
				<?php
					while($dado= $db->Fetch()){
						$photo = new Photo();
						$photo->loadByDate($dado->date);

						$dateFinal = strftime( '%B/%Y', strtotime( date("M",strtotime($photo->getDate())) ) );

						if($datePost == $photo->getIdphoto()) {
							$selected = "selected";
						} else {
							$selected = "";
						}
						echo "<li><a href=\"index.php?t=calendar-images&room=$idroom&type=$type&date={$photo->getDate()}\">$dateFinal</a></li>";
					}
				?>
				</ul>
				<div class="images">
					<div class="highlight carousel">
					<?php
						while($dadop= $dbp->Fetch()){
							$photo = new Photo();
							$photo->Carrega($dadop->idphoto);

							echo "
							<div class=\"carousel-content\">
								<div class=\"image-box\">
									<img src=\"images/{$photo->getFile()}\" class=\"intense\">
								</div>
								<div class=\"bottom\">
									<h2>{$photo->getTitle()}</h2>
									<p>{$photo->getDescription()}</p>
								</div>
							</div>"
								;
						}
					?>
				</div>
				<div class="controls-thumbs">
					<div class="thumbs-container" style="">
						<?php
							while($dadop2= $dbp2->Fetch()){
								$photo = new Photo();
								$photo->Carrega($dadop2->idphoto);

								echo "
								<div class=\"image-box\">
									<a href=\"#\"><img src=\"images/{$photo->getFile()}\"></a>
								</div>
								";
							}
						?>
					</div>
				</div>
				<div class="customNavigation">
					<a class="prevpic">Previous</a>
					<a class="nextpic">Next</a>
				</div>
			</div>
		</div>
	</div>
</div>