<?php 
	if(empty($_GET['build'])) {
		Util::Redireciona("index.php");
		exit();
	} else {
		$idbuild = (int) $_GET['build'];
		$build = new Build();
		$build->Carrega($idbuild);
	}
	if(empty($_GET['type'])) {
		$type = 0;
	} else {
		$type = (int) $_GET['type'];
	}
?>

<div id="photos-new" class="section" ng-class="{'show':sectionTab==='login'}">
	<div class="wrapper">
		<h1 class="title"><?php echo $build->getName(); ?></h1>
		<h2 class="subtitle">Adicionar <?php if ($type == 1) { echo "mídia"; } else { echo "fotos"; } ?></h2>
		<div class="top-content">
			<form action="controllers/photos-save.php" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="idbuild" value="<?php echo $idbuild; ?>">
				<input type="hidden" name="type" value="<?php echo $type; ?>">

				<span class="input-content date">
					<label>
						<span class="title">Selecione o mês:</span>
						<select name="month">
							<option value="01">Janeiro</option>
							<option value="02">Fevereiro</option>
							<option value="03">Março</option>
							<option value="04">Abril</option>
							<option value="05">Maio</option>
							<option value="06">Junho</option>
							<option value="07">Julho</option>
							<option value="08">Agosto</option>
							<option value="09">Setembro</option>
							<option value="10">Outubro</option>
							<option value="11">Novembro</option>
							<option value="12">Dezembro</option>
						</select>
					</label>
					<label>
						<span class="title">Selecione o ano:</span>
						<select name="year">
							<?php
								for($i = date("Y"); $i > 2000; $i--) {
									echo "<option value='$i'>$i</option>";
								}
							?>
						</select>
					</label>
				</span>
				<span class="input-content">
					<label>
						<span class="title">Titulo</span>
						<input type="text" name="title[]" placeholder="Titulo">
					</label>
					<label>
						<span class="title">Descrição</span>
						<input type="text" name="description[]" placeholder="Descrição da foto">
					</label>
					<label>
						<span class="title">Escolher imagem:</span>
						<span class="file-bg">
							<input class="custom-file-input" type="file" name="photo[]">
						</span>
					</label>
				</span>

				<span class="input-content">
					<label>
						<span class="title">Titulo</span>
						<input type="text" name="title[]" placeholder="Titulo">
					</label>
					<label>
						<span class="title">descrição</span>
						<input type="text" name="description[]" placeholder="Descrição da foto">
					</label>
					<label>
						<span class="title">Escolher imagem:</span>
						<span class="file-bg">
							<input class="custom-file-input" type="file" name="photo[]">
						</span>
					</label>
				</span>

				<span class="input-content">
					<label>
						<span class="title">Titulo</span>
						<input type="text" name="title[]" placeholder="Titulo">
					</label>
					<label>
						<span class="title">descrição</span>
						<input type="text" name="description[]" placeholder="Descrição da foto">
					</label>
					<label>
						<span class="title">Escolher imagem:</span>
						<span class="file-bg">
							<input class="custom-file-input" type="file" name="photo[]">
						</span>
					</label>
				</span>

				<span class="input-content">
					<label>
						<span class="title">Titulo</span>
						<input type="text" name="title[]" placeholder="Titulo">
					</label>
					<label>
						<span class="title">descrição</span>
						<input type="text" name="description[]" placeholder="Descrição da foto">
					</label>
					<label>
						<span class="title">Escolher imagem:</span>
						<span class="file-bg">
							<input class="custom-file-input" type="file" name="photo[]">
						</span>
					</label>
				</span>

				<span class="input-content">					
					<button class="enter" ng-click="sectionTab = 'calendar'">Salvar</button>
				</span>
			</form>
		</div>
	</div>
</div>