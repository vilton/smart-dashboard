<?php 
	$query= "SELECT * 
			 FROM Build";
	$db= new DB();
	$db->Sql($query);
?>

<div id="builds">
	<div class="builds-content">
		<h1 class="title">Lista de obras</h1>
		<a class="button" href="index.php?t=build-new">Adicionar</a>
			<table>
				<thead>
					<th>Nome</th>
					<th>Salas</th>
					<th>Andamento</th>
					<th>Fotos</th>
					<th>Mídia</th>
					<th></th>
				</thead>
			<tbody>
		<?php
			while($dado= $db->Fetch()){
				$build = new Build();
				$build->Carrega($dado->idbuild);
				echo "
			<tr>
				<td>
					<a href='index.php?t=build-new&build={$build->getIdbuild()}'>{$build->getName()}</a>
				</td>
				<td>
					<a href='index.php?t=rooms&build={$build->getIdbuild()}'>Ver</a>
				</td>
				<td>
					<a href='index.php?t=statistics-new&build={$build->getIdbuild()}'>Adicionar</a>
				</td>
				<td>
					<a href='index.php?t=photos-new&build={$build->getIdbuild()}'>Adicionar</a>
				</td>
				<td>
					<a href='index.php?t=photos-new&type=1&build={$build->getIdbuild()}'>Adicionar</a>
				</td>
				<td>
					<a href='index.php?t=build-del&build={$build->getIdbuild()}'>Deletar</a>
				</td>
			</tr>";
			}
		?>
			<!-- <tr class="footer">
				<td>Total</td>
				<td>R$9999.00,00</td>
			</tr> -->
			</tbody>
		</table>
	</div>
</div>