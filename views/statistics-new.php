<?php 
	if(empty($_GET['build'])) {
		Util::Redireciona("index.php");
		exit();
	} else {
		$idbuild = (int) $_GET['build'];
		$build = new Build();
		$build->Carrega($idbuild);
	}
?>
<form action="controllers/statistics-save.php" method="POST">
<input type="hidden" name="idbuild" value="<?php echo $idbuild ?>">
<div id="statistics-new">
	<div class="content">
		<h1 class="title"><?php echo $build->getName(); ?></h1>
		<h2 class="subtitle">Cadastro de andamento</h2>
		<div class="tasks">
			<select name="year">
				<?php
					for($i = date("Y"); $i > 2000; $i--) {
						echo "<option value='$i'>$i</option>";
					}
				?>
			</select>
			<ul>
				<li>Projeto e serviços técnicos</li>
				<li>Preparação do terreno</li>
				<li>Fundações</li>
				<li>Escavação</li>
				<li>Estruturas</li>
				<li>Alvenaria</li>
				<li>Esquadrias</li>
				<li>Impermeabilização</li>
				<li>Revestimentos e forros</li>
				<li>Pavimentação</li>
				<li>Instalações elétricas</li>
				<li>Paisagismo</li>
				<li>Decoração</li>
			</ul>
		</div>
		<div class="progress-container">
			<select name="month">
				<option value="01">Janeiro</option>
				<option value="02">Fevereiro</option>
				<option value="03">Março</option>
				<option value="04">Abril</option>
				<option value="05">Maio</option>
				<option value="06">Junho</option>
				<option value="07">Julho</option>
				<option value="08">Agosto</option>
				<option value="09">Setembro</option>
				<option value="10">Outubro</option>
				<option value="11">Novembro</option>
				<option value="12">Dezembro</option>
			</select>
			<ul class="progress-bar-container">
				<li class="progress-input"><input type="text" name="projects"></li>
				<li class="progress-input"><input type="text" name="ground"></li>
				<li class="progress-input"><input type="text" name="foundations"></li>
				<li class="progress-input"><input type="text" name="excavation"></li>
				<li class="progress-input"><input type="text" name="structures"></li>
				<li class="progress-input"><input type="text" name="masonry"></li>
				<li class="progress-input"><input type="text" name="frames"></li>
				<li class="progress-input"><input type="text" name="waterproofing"></li>
				<li class="progress-input"><input type="text" name="coatings"></li>
				<li class="progress-input"><input type="text" name="paving"></li>
				<li class="progress-input"><input type="text" name="electrical"></li>
				<li class="progress-input"><input type="text" name="landscaping"></li>
				<li class="progress-input"><input type="text" name="decoration"></li>
			</ul>
		</div>
		<button class="save">Salvar</button>
	</div>
</div>
</form>