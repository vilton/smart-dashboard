<?php 
	$query= "SELECT 
				* 
			 FROM 
			 	User";
	$db= new DB();
	$db->Sql($query);
?>

<div id="rooms">
	<div class="rooms-content">
		<h1 class="title">Lista de usuários</h1>
		<a class="button" href="index.php?t=user-new">Adicionar</a>
		<table>
			<thead>
				<th>ID</th>
				<th>Nome</th>
				<th></th>
				<th></th>
			</thead>
		<tbody>
		<?php
			while($dado= $db->Fetch()){
				$user = new User();
				$user->Carrega($dado->iduser);
				echo "
			<tr>
				<td>
					{$user->getIduser()}
				</td>
				<td>
					<a href='index.php?t=edit-user&user={$user->getIduser()}'>{$user->getEmail()}</a>
				</td>
				<td>
					<a href='index.php?t=user-room&user={$user->getIduser()}'>Adicionar Sala</a>
				</td>
				<td>
					<a href=''>Deletar</a>
				</td>
			</tr>";
			}
		?>
			<!-- <tr class="footer">
				<td>Total</td>
				<td>R$9999.00,00</td>
			</tr> -->
			</tbody>
		</table>
	</div>
</div>