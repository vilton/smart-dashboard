<?php 
	if(empty($idbuild)) {
		$idbuild = $build_index;
	}

	$datePost = $_POST['date'];
	$progress = new Progress();

	if(empty($datePost)) {
		$progress->carregaBuild($idbuild);
	} else {
		$progress->Carrega($datePost);
	}

	// $build = new Build();
	// $build->Carrega($idbuild);

	$query= "SELECT 
					DISTINCT(date), idprogress
			 FROM 
			 	Progress
			 WHERE 
			 	Build_idbuild = '$idbuild'
			 ORDER BY
			 	date
			 DESC";
	$db= new DB();
	$db->Sql($query);
?>
<div id="statistics">
	<div class="left">
		<div class="title-pdf">
			<h1 class="title"><?php echo $build->getName() ?></h1>
			<a class="download-pdf button" style="display: none">Download PDF</a>
		</div>
		<div class="tasks">
			<form action="" method="POST">
				<select name="date" onchange="this.form.submit()">
				<?php
					while($dado= $db->Fetch()){
						$progress2 = new Progress();
						$progress2->Carrega($dado->idprogress);

						$dateFinal = strftime( '%B/%Y', strtotime( date("M",strtotime($progress2->getDate())) ) );

						if($datePost == $progress2->getIdprogress()) {
							$selected = "selected";
						} else {
							$selected = "";
						}
						echo "<option value=\"{$progress2->getIdprogress()}\" $selected>$dateFinal</option>";
					}
				?>
				</select>
			</form>
			<ul>
				<li>Projeto e serviços técnicos</li>
				<li>Preparação do terreno</li>
				<li>Fundações</li>
				<li>Escavação</li>
				<li>Estruturas</li>
				<li>Alvenaria</li>
				<li>Esquadrias</li>
				<li>Impermeabilização</li>
				<li>Revestimentos e forros</li>
				<li>Pavimentação</li>
				<li>Instalações elétricas</li>
				<li>Paisagismo</li>
				<li>Decoração</li>
			</ul>
		</div>
		<div class="progress-container">
			<ul class="percents">
				<li>0%</li>
				<li>10%</li>
				<li>20%</li>
				<li>30%</li>
				<li>40%</li>
				<li>50%</li>
				<li>60%</li>
				<li>70%</li>
				<li>80%</li>
				<li>90%</li>
				<li>100%</li>
			</ul>
			<ul class="progress-bar-container">
				<li class="progress-bar" data-value="<?php echo $progress->getProjects() / 10 * 0.10 ?>"></li>
				<li class="progress-bar" data-value="<?php echo $progress->getGround() / 10 * 0.10 ?>"></li>
				<li class="progress-bar" data-value="<?php echo $progress->getFoundations() / 10 * 0.10 ?>"></li>
				<li class="progress-bar" data-value="<?php echo $progress->getExcavation() / 10 * 0.10 ?>"></li>
				<li class="progress-bar" data-value="<?php echo $progress->getStructures() / 10 * 0.10 ?>"></li>
				<li class="progress-bar" data-value="<?php echo $progress->getMasonry() / 10 * 0.10 ?>"></li>
				<li class="progress-bar" data-value="<?php echo $progress->getFrames() / 10 * 0.10 ?>"></li>
				<li class="progress-bar" data-value="<?php echo $progress->getWaterproofing() / 10 * 0.10 ?>"></li>
				<li class="progress-bar" data-value="<?php echo $progress->getCoatings() / 10 * 0.10 ?>"></li>
				<li class="progress-bar" data-value="<?php echo $progress->getPaving() / 10 * 0.10 ?>"></li>
				<li class="progress-bar" data-value="<?php echo $progress->getElectrical() / 10 * 0.10 ?>"></li>
				<li class="progress-bar" data-value="<?php echo $progress->getLandscaping() / 10 * 0.10 ?>"></li>
				<li class="progress-bar" data-value="<?php echo $progress->getDecoration() / 10 * 0.10 ?>"></li>
			</ul>
		</div>

	</div>
	<div class="right side-bar">
		<div class="vertical-align">
			<h2>Status Geral:</h2>
			<div class="progress-circle-container">
				<div class="progress-circle" data-value="<?php echo $build->getStats() / 10 * 0.10 ?>"></div>
				<span class="percent-data"><?php echo $build->getStats() ?>%</span>
			</div>
			<div class="text">
				<span class="exclamation-ico"></span>
				<h2>Observações</h2>
				<p><?php echo $build->getDescription() ?></p>
			</div>
		</div>
	</div>
</div>