<div id="financial-new">
	<h1 class="title">Cadastro Financeiro</h1>
	<h2 class="subtitle">Envie sua tabela com os dados do empreendimento</h2>
	<form action="controllers/financial-save.php" method="POST" enctype="multipart/form-data">
		<span class="file-bg">
			<input type="file" name="financial" required class="custom-file-input">
		</span>

		<button class="enter">Salvar</button>
	</form>
</div>