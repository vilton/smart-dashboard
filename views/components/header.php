<header id="header" class="section">
	<a class="logo" href="index.php"></a>
	<ul class="menu">
		<li>
			<a href="index.php?t=statistics&room=<?php echo $room->getIdroom() ?>"><?php echo "{$build->getName()} / {$room->getName()}"; ?></a>
			<span class="ico-dropdown"></span>
			<ul class="dropdown">
			<?php
				while($dado_menu= $db_menu->Fetch()){
					$room_menu = new Room();
					$room_menu->Carrega($dado_menu->Room_idroom);

					$build_menu = new Build();
					$build_menu->Carrega($room_menu->getBuild_idbuild());
					echo "<li><a href=\"index.php?t=statistics&room={$room_menu->getIdRoom()}\">{$build_menu->getName()} / {$room_menu->getName()}</a></li>";
				}
			?>
			</ul>
		</li>
		<!-- <li>
			<a href="index.php?t=financial&room=<?php echo $room->getIdroom() ?>"><?php echo $room->getName() ?></a>
			<ul class="dropdown">
				<li><a href="">Outra Sala</a></li>
				<li><a href="">Outra Sala</a></li>
			</ul>
		</li> -->
	</ul>

	<ul class="user">
		<li>
			<a href="controllers/logoff.php">Sair</a>
		</li>
	</ul>

	<ul class="nav">
		<li><a <?php if($_GET['t'] == "statistics") { echo "class='active'"; } ?> href="index.php?t=statistics&room=<?php echo $room->getIdroom() ?>">Andamento</a></li>
		<li><a <?php if($_GET['t'] == "calendar-images" && $_GET['type'] != 1) { echo "class='active'"; } ?>href="index.php?t=calendar-images&room=<?php echo $room->getIdroom() ?>" ng-click="sectionTab ='slides'">Fotos</a></li>
		<li><a <?php if($_GET['t'] == "financial") { echo "class='active'"; } ?>href="index.php?t=financial&room=<?php echo $room->getIdroom() ?>" ng-click="sectionTab ='financial'">Financeiro</a></li>
		<li><a <?php if($_GET['t'] == "calendar-images" && $_GET['type'] == 1) { echo "class='active'"; } ?>href="index.php?t=calendar-images&type=1&room=<?php echo $room->getIdroom() ?>" ng-click="sectionTab ='calendar'">Mídia</a></li>
	</ul>

</header>