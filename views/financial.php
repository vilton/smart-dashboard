<?php
	// $idroom = (int) $_GET['room'];

	$query= "SELECT 
				* 
			 FROM
			 	Financial
			 WHERE
			 	Room_ref = '{$room->getReference()}'";
	$db= new DB();
	$db->Sql($query);

	// $room = new Room();
	// $room->Carrega($idroom);

	// $build = new Build();
	// $build->Carrega($room->getBuild_idbuild());
?>
<div id="financial">
	<div class="title-pdf">
		<h1 class="title"><?php echo $build->getName() ?> - <?php echo $room->getName() ?></h1>
		<a class="download-pdf button" style="display: none">Download PDF</a>
	</div>
	<div class="left">
		<table>
				<thead>
					<th>Periodo</th>
					<th>INCC</th>
					<th>Valor parcela em INCSS'S</th>
					<th>Parcela</th>
					<th>Pago</th>
					<th>Vencimento</th>
				</thead>
			<tbody>
			<?php
				while($dado= $db->Fetch()){
					$financial = new Financial();
					$financial->Carrega($dado->idfinancial);
					echo "
				<tr>
					<td>{$financial->getPeriod()}</td>
					<td>{$financial->getIncc()}</td>
					<td>{$financial->getIncss()}</td>
					<td>{$financial->getParcel()}</td>
					<td>{$financial->getPaid()}</td>
					<td>{$financial->getDue()}</td>
				</tr>";
				}
			?>
			<tr class="footer">
				<td>Total</td>
				<td>R$9999.00,00</td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			</tbody>
		</table>
	</div>
	<div class="right side-bar">
		<div class="vertical-align">
			<div class="info">
				<h2>Status Geral:</h2>
				<p>Dados Bancários:</p>
				<p>Banco do Brasil-</p>
				<p>Sm Art Conceituação ltda me</p>
				<p>cc 91769-9 ag 2814-2</p>
				<p>cnpj: 10193713/0002-53</p>
				<p>INCC JUNHO/2015 631,747</p>
			</div>
		</div>
	</div>
</div>