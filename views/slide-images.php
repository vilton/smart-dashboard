<div id="slide-images">
	<div class="content vertical-align">
		<div class="wrapper">
			<div class="carousel">
				<div class="carousel-item">
					<div class="top-content">
						<div class="images">
							<div class="highlight">
								<div class="image-box">
									<img src="http://placehold.it/500x350">
								</div>
							</div>
						</div>
					</div>
					<div class="bottom">
						<h2>Legenda</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat.</p>
					</div>
				</div>
				<div class="carousel-item">
					<div class="top-content">
						<div class="images">
							<div class="highlight">
								<div class="image-box">
									<img src="http://placehold.it/500x350">
								</div>
							</div>
						</div>
					</div>
					<div class="bottom">
						<h2>Legenda</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat.</p>
					</div>
				</div>
				<div class="carousel-item">
					<div class="top-content">
						<div class="images">
							<div class="highlight">
								<div class="image-box">
									<img src="http://placehold.it/500x350">
								</div>
							</div>
						</div>
					</div>
					<div class="bottom">
						<h2>Legenda</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
