<?php 
	$idbuild = (int) $_GET['build'];

	$build = new Build();
	$build->Carrega($idbuild);

	$query= "SELECT 
				* 
			 FROM 
			 	Room
			 WHERE 
			 	Build_idbuild = '$idbuild'";
	$db= new DB();
	$db->Sql($query);
?>

<div id="rooms">
	<div class="rooms-content">
		<h1 class="title"><?php echo $build->getName(); ?></h1>
		<h2 class="subtitle">Lista de salas</h2>
		<a class="button" href="index.php?t=room-new&build=<?php echo $build->getIdbuild(); ?>">Adicionar</a>
		<table>
			<thead>
				<th>ID</th>
				<th>REF</th>
				<th>Nome</th>
				<th></th>
			</thead>
		<tbody>
		<?php
			while($dado= $db->Fetch()){
				$room = new Room();
				$room->Carrega($dado->idroom);
				echo "
			<tr>
				<td>{$room->getIdroom()}</td>
				<td>{$room->getReference()}</td>
				<td>{$room->getName()}</td>
				<td>
					<a href=''>Deletar</a>
				</td>
			</tr>";
			}
		?>
			<!-- <tr class="footer">
				<td>Total</td>
				<td>R$9999.00,00</td>
			</tr> -->
			</tbody>
		</table>
	</div>
</div>