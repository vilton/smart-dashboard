<?php
	$iduser = (int) $_GET['user'];

	$user = new User();
	$user->Carrega($iduser);
?>
<div id="user-new">
	<div class="user-new-content">
		<h1 class="title"><?php echo $user->getEmail() ?></h1>
		<h2 class="subtitle">Editar usuário</h1>
		<div>
			<form action="controllers/user-save.php" method="POST">
				<input type="hidden" name="user" value="<?php echo $iduser ?>">
				<input type="hidden" name="edit" value="1">
				<label>
					<span>CPF</span>
					<input type="text" name="cpf" required value="<?php echo $user->getCpf() ?>">
				</label>
				<label>
					<span>E-mail</span>
					<input type="text" name="email" required value="<?php echo $user->getEmail() ?>">
				</label>
				<button class="enter">OK</button>
			</form>
		</div>
	</div>
</div>