<?php
	class User {
		private $iduser;
		private $cpf;
		private $email;
		private $idroom;

		public function __Construct(){
			$this->iduser= 0;
			$this->cpf= '';
			$this->email= '';
			$this->idroom= 0;
		}

		public function getIduser() {
			return $this->iduser;
		}
		public function setIduser($iduser) {
			return $this->iduser = (int) $iduser;
		}
		public function getCpf() {
			return $this->cpf;
		}
		public function setCpf($cpf) {
			return $this->cpf = $cpf;
		}
		public function getEmail() {
			return $this->email;
		}
		public function setEmail($email) {
			return $this->email = $email;
		}
		public function getIdroom() {
			return $this->idroom;
		}
		public function setIdroom($idroom) {
			return $this->idroom = $idroom;
		}
		public function Carrega($id){
			$id= (int) $id;

			$query= "SELECT * 
					 FROM User
					 WHERE iduser = {$id}";
			$db= new DB();
			$db->Sql($query);

			if($db->NumRows() == 0){
				throw new Exception('Invalido');
			}

			$dado= $db->Fetch();

			$this->setIduser($dado->iduser);
			$this->setCpf($dado->cpf);
			$this->setEmail($dado->email);
		}

		public function Cadastra(){
			$db= new DB();
			$query = "INSERT INTO User (
						cpf,
						email
					)VALUES(
						'{$this->getCpf()}',
						'{$this->getEmail()}'
					)";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao cadastrar');
			}

			$this->setIduser($db->LastInsertId());
		}

		public function CadastraSala(){
			$db= new DB();
			$query = "INSERT INTO User_has_Room (
						User_iduser,
						Room_idroom
					)VALUES(
						'{$this->getIduser()}',
						'{$this->getIdroom()}'
					)";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao cadastrar');
			}

			$this->setIduser($db->LastInsertId());
		}

		public function Edita(){
			$db= new DB();
			$query = "UPDATE User SET
						cpf= '{$this->getCpf()}',
						email= '{$this->getEmail()}'
					WHERE iduser = {$this->getIduser()}";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao editar');
			}
		}

		public function Remove(){
			$db= new DB();
			$query = "DELETE FROM User 
						WHERE iduser = {$this->getIduser()}
						LIMIT 1";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao remover');
			}
		}
	}
?>