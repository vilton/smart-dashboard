<?php
	class Progress {
		private $idprogress;
		private $date;
		private $projects;
		private $ground;
		private $foundations;
		private $excavation;
		private $structures;
		private $masonry;
		private $frames;
		private $waterproofing;
		private $coatings;
		private $paving;
		private $electrical;
		private $landscaping;
		private $decoration;
		private $Build_idbuild;

		public function __Construct(){
			$this->idprogress = 0;
			$this->date = '';
			$this->projects = '';
			$this->ground = '';
			$this->foundations = '';
			$this->excavation = '';
			$this->structures = '';
			$this->masonry = '';
			$this->frames = '';
			$this->waterproofing = '';
			$this->coatings = '';
			$this->paving = '';
			$this->electrical = '';
			$this->landscaping = '';
			$this->decoration = '';
			$this->Build_idbuild = '';
		}

		public function getIdprogress() {
			return $this->idprogress;
		}
		public function setIdprogress($idprogress) {
			return $this->idprogress = $idprogress;
		}
		public function getDate() {
			return $this->date;
		}
		public function setDate($date) {
			return $this->date = $date;
		}
		public function getProjects() {
			return $this->projects;
		}
		public function setProjects($projects) {
			return $this->projects = $projects;
		}
		public function getGround() {
			return $this->ground;
		}
		public function setGround($ground) {
			return $this->ground = $ground;
		}
		public function getFoundations() {
			return $this->foundations;
		}
		public function setFoundations($foundations) {
			return $this->foundations = $foundations;
		}
		public function getExcavation() {
			return $this->excavation;
		}
		public function setExcavation($excavation) {
			return $this->excavation = $excavation;
		}
		public function getStructures() {
			return $this->structures;
		}
		public function setStructures($structures) {
			return $this->structures = $structures;
		}
		public function getMasonry() {
			return $this->masonry;
		}
		public function setMasonry($masonry) {
			return $this->masonry = $masonry;
		}
		public function getFrames() {
			return $this->frames;
		}
		public function setFrames($frames) {
			return $this->frames = $frames;
		}
		public function getWaterproofing() {
			return $this->waterproofing;
		}
		public function setWaterproofing($waterproofing) {
			return $this->waterproofing = $waterproofing;
		}
		public function getCoatings() {
			return $this->coatings;
		}
		public function setCoatings($coatings) {
			return $this->coatings = $coatings;
		}
		public function getPaving() {
			return $this->paving;
		}
		public function setPaving($paving) {
			return $this->paving = $paving;
		}
		public function getElectrical() {
			return $this->electrical;
		}
		public function setElectrical($electrical) {
			return $this->electrical = $electrical;
		}
		public function getLandscaping() {
			return $this->landscaping;
		}
		public function setLandscaping($landscaping) {
			return $this->landscaping = $landscaping;
		}
		public function getDecoration() {
			return $this->decoration;
		}
		public function setDecoration($decoration) {
			return $this->decoration = $decoration;
		}
		public function getBuild_idbuild() {
			return $this->Build_idbuild;
		}
		public function setBuild_idbuild($Build_idbuild) {
			return $this->Build_idbuild = $Build_idbuild;
		}

		public function Carrega($id){
			$id= (int) $id;

			$query= "SELECT 
						* 
					 FROM 
					 	Progress
					 WHERE 
					 	idprogress = {$id}";
			$db= new DB();
			$db->Sql($query);

			if($db->NumRows() == 0){
				throw new Exception('Invalido');
			}

			$dado= $db->Fetch();

			$this->setIdprogress($dado->idprogress);
			$this->setDate($dado->date);
			$this->setProjects($dado->projects);
			$this->setGround($dado->ground);
			$this->setFoundations($dado->foundations);
			$this->setExcavation($dado->excavation);
			$this->setStructures($dado->structures);
			$this->setMasonry($dado->masonry);
			$this->setFrames($dado->frames);
			$this->setWaterproofing($dado->waterproofing);
			$this->setCoatings($dado->coatings);
			$this->setPaving($dado->paving);
			$this->setElectrical($dado->electrical);
			$this->setLandscaping($dado->landscaping);
			$this->setDecoration($dado->decoration);
			$this->setBuild_idbuild($dado->Build_idbuild);
		}

		public function carregaBuild($id){
			$id= (int) $id;

			$query= "SELECT 
						* 
					 FROM 
					 	Progress
					 WHERE 
					 	Build_idbuild = {$id}
					 ORDER BY
					 	date 
					 DESC
					 LIMIT
					 	1";
			$db= new DB();
			$db->Sql($query);

			if($db->NumRows() == 0){
				throw new Exception('Invalido');
			}

			$dado= $db->Fetch();

			$this->setIdprogress($dado->idprogress);
			$this->setDate($dado->date);
			$this->setProjects($dado->projects);
			$this->setGround($dado->ground);
			$this->setFoundations($dado->foundations);
			$this->setExcavation($dado->excavation);
			$this->setStructures($dado->structures);
			$this->setMasonry($dado->masonry);
			$this->setFrames($dado->frames);
			$this->setWaterproofing($dado->waterproofing);
			$this->setCoatings($dado->coatings);
			$this->setPaving($dado->paving);
			$this->setElectrical($dado->electrical);
			$this->setLandscaping($dado->landscaping);
			$this->setDecoration($dado->decoration);
			$this->setBuild_idbuild($dado->Build_idbuild);
		}

		public function Cadastra(){
			$db= new DB();

			$query = "INSERT INTO Progress (
						date,
						projects,
						ground,
						foundations,
						excavation,
						structures,
						masonry,
						frames,
						waterproofing,
						coatings,
						paving,
						electrical,
						landscaping,
						decoration,
						Build_idbuild
					)VALUES(
						'{$this->getDate()}',
						'{$this->getProjects()}',
						'{$this->getGround()}',
						'{$this->getFoundations()}',
						'{$this->getExcavation()}',
						'{$this->getStructures()}',
						'{$this->getMasonry()}',
						'{$this->getFrames()}',
						'{$this->getWaterproofing()}',
						'{$this->getCoatings()}',
						'{$this->getPaving()}',
						'{$this->getElectrical()}',
						'{$this->getLandscaping()}',
						'{$this->getDecoration()}',
						'{$this->getBuild_idbuild()}'
					)";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao cadastrar');
			}

			$this->setIdprogress($db->LastInsertId());
		}

		public function Edita(){
			$db= new DB();
			$query = "UPDATE Progress SET
						date = '{$this->getDate()}',
						projects = '{$this->getProjects()}',
						ground = '{$this->getGround()}',
						foundations = '{$this->getFoundations()}',
						excavation = '{$this->getExcavation()}',
						structures = '{$this->getStructures()}',
						masonry = '{$this->getMasonry()}',
						frames = '{$this->getFrames()}',
						waterproofing = '{$this->getWaterproofing()}',
						coatings = '{$this->getCoatings()}',
						paving = '{$this->getPaving()}',
						electrical = '{$this->getElectrical()}',
						landscaping = '{$this->getLandscaping()}',
						decoration = '{$this->getDecoration()}',
						Build_idbuild = '{$this->getBuild_idbuild()}'
					WHERE idprogress = {$this->getIdprogress()}";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao editar');
			}
		}

		public function Remove(){
			$db= new DB();
			$query = "DELETE FROM Progress 
						WHERE idprogress = {$this->getIdprogress()}
						LIMIT 1";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao remover');
			}
		}
	}