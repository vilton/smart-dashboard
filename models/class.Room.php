<?php
	class Room {
		private $idroom;
		private $name;
		private $reference;
		private $Build_idbuild;
		private $User_iduser;

		public function __Construct(){
			$this->idroom= 0;
			$this->name= '';
			$this->Build_idbuild = 0;
			$this->User_iduser = 0;
		}

		public function getIdroom() {
			return $this->idroom;
		}
		public function setIdroom($idroom) {
			return $this->idroom = (int) $idroom;
		}
		public function getName() {
			return $this->name;
		}
		public function setName($name) {
			return $this->name = $name;
		}
		public function getReference() {
			return $this->reference;
		}
		public function setReference($reference) {
			return $this->reference = $reference;
		}
		public function getBuild_idbuild() {
			return $this->Build_idbuild;
		}
		public function setBuild_idbuild($idbuild) {
			return $this->Build_idbuild = $idbuild;
		}
		public function getUser_iduser() {
			return $this->User_iduser;
		}
		public function setUser_iduser($iduser) {
			return $this->User_iduser = $iduser;
		}

		public function Carrega($id){
			$id= (int) $id;

			$query= "SELECT * 
					 FROM Room
					 WHERE idroom = {$id}";
			$db= new DB();
			$db->Sql($query);

			if($db->NumRows() == 0){
				throw new Exception('Invalido');
			}

			$dado= $db->Fetch();

			$this->setIdroom($dado->idroom);
			$this->setName($dado->name);
			$this->setReference($dado->reference);
			$this->setBuild_idbuild($dado->Build_idbuild);
		}

		public function Cadastra(){
			$db= new DB();
			$query = "INSERT INTO Room (
						name,
						reference,
						Build_idbuild,
						User_iduser
					)VALUES(
						'{$this->getName()}',
						'{$this->getReference()}',
						'{$this->getBuild_idbuild()}',
						'{$this->getUser_iduser()}'
					)";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao cadastrar');
			}

			$this->setIdroom($db->LastInsertId());
		}

		public function Edita(){
			$db= new DB();
			$query = "UPDATE Room SET
						name= '{$this->getName()}',
						Build_idbuild= '{$this->getBuild_idbuild()}'
					WHERE idroom = {$this->getidroom()}";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao editar');
			}
		}

		public function Remove(){
			$db= new DB();
			$query = "DELETE FROM Room 
						WHERE idroom = {$this->getidroom()}
						LIMIT 1";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao remover');
			}
		}
	}