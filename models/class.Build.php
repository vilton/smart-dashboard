<?php
	class Build {
		private $idbuild;
		private $name;
		private $description;
		private $stats;

		public function __Construct(){
			$this->idbuild= 0;
			$this->name= '';
			$this->description= '';
			$this->stats= '';
		}

		public function getIdbuild() {
			return $this->idbuild;
		}
		public function setIdbuild($idbuild) {
			return $this->idbuild = (int) $idbuild;
		}
		public function getName() {
			return $this->name;
		}
		public function setName($name) {
			return $this->name = $name;
		}
		public function getDescription() {
			return $this->description;
		}
		public function setDescription($description) {
			return $this->description = $description;
		}
		public function getStats() {
			return $this->stats;
		}
		public function setStats($stats) {
			return $this->stats = $stats;
		}

		public function Carrega($id){
			$id= (int) $id;

			$query= "SELECT * 
					 FROM Build
					 WHERE idbuild = {$id}";
			$db= new DB();
			$db->Sql($query);

			if($db->NumRows() == 0){
				throw new Exception('Invalido');
			}

			$dado= $db->Fetch();

			$this->setIdbuild($dado->idbuild);
			$this->setName($dado->name);
			$this->setDescription($dado->description);
			$this->setStats($dado->stats);
		}

		public function Cadastra(){
			$db= new DB();
			$query = "INSERT INTO Build (
						name,
						description,
						stats
					)VALUES(
						'{$this->getName()}',
						'{$this->getDescription()}',
						'{$this->getStats()}'
					)";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao cadastrar');
			}

			$this->setIdbuild($db->LastInsertId());
		}

		public function Edita(){
			$db= new DB();
			$query = "UPDATE Build SET
						name= '{$this->getName()}',
						description= '{$this->getDescription()}',
						stats= '{$this->getStats()}'
					WHERE idbuild = {$this->getIdbuild()}";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao editar');
			}
		}

		public function Remove(){
			$db= new DB();
			$query = "DELETE FROM Build 
						WHERE idbuild = {$this->getIdbuild()}
						LIMIT 1";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao remover');
			}
		}
	}