<?php
	class Financial {
		private $idfinancial;
		private $period;
		private $incc;
		private $contract;
		private $incss;
		private $parcel;
		private $paid;
		private $maturity;
		private $due;
		private $Room_ref;

		public function __Construct(){
			$this->idfinancial= 0;
			$this->period= '';
			$this->incc= '';
			$this->contract= '';
			$this->incss= '';
			$this->parcel= '';
			$this->paid= '';
			$this->maturity= '';
			$this->due= '';
			$this->Room_ref= 0;
		}

		public function getIdfinancial() {
			return $this->idfinancial;
		}
		public function setIdfinancial($idfinancial) {
			return $this->idfinancial = (int) $idfinancial;
		}

		public function getPeriod() {
			return $this->period;
		}
		public function setPeriod($period) {
			return $this->period = $period;
		}

		public function getIncc() {
			return $this->incc;
		}
		public function setIncc($incc) {
			return $this->incc = $incc;
		}

		public function getContract() {
			return $this->contract;
		}
		public function setContract($contract) {
			return $this->contract = $contract;
		}

		public function getIncss() {
			return $this->incss;
		}
		public function setIncss($incss) {
			return $this->incss = $incss;
		}

		public function getParcel() {
			return $this->parcel;
		}
		public function setParcel($parcel) {
			return $this->parcel = $parcel;
		}

		public function getPaid() {
			return $this->paid;
		}
		public function setPaid($paid) {
			return $this->paid = $paid;
		}

		public function getMaturity() {
			return $this->maturity;
		}

		public function setMaturity($maturity) {
			return $this->maturity = $maturity;
		}

		public function getDue() {
			return $this->due;
		}
		public function setDue($due) {
			return $this->due = $due;
		}

		public function getRoom_ref() {
			return $this->Room_ref;
		}
		public function setRoom_ref($Room_ref) {
			return $this->Room_ref = $Room_ref;
		}

		public function Carrega($id){
			$id= (int) $id;

			$query= "SELECT * 
					 FROM Financial
					 WHERE idfinancial = {$id}";
			$db= new DB();
			$db->Sql($query);

			if($db->NumRows() == 0){
				throw new Exception('Invalido');
			}

			$dado= $db->Fetch();

			$this->setIdfinancial($dado->idfinancial);
			$this->setPeriod($dado->period);
			$this->setIncc($dado->incc);
			$this->setContract($dado->contract);
			$this->setIncss($dado->incss);
			$this->setParcel($dado->parcel);
			$this->setPaid($dado->paid);
			$this->setMaturity($dado->maturity);
			$this->setDue($dado->due);
			$this->setRoom_ref($dado->Room_ref);
		}

		public function Cadastra(){
			$db= new DB();
			$query = "INSERT INTO Financial (
						period,
						incc,
						contract,
						incss,
						parcel,
						paid,
						maturity,
						due,
						Room_ref
					)VALUES(
						'{$this->getPeriod()}',
						'{$this->getIncc()}',
						'{$this->getContract()}',
						'{$this->getIncss()}',
						'{$this->getParcel()}',
						'{$this->getPaid()}',
						'{$this->getMaturity()}',
						'{$this->getDue()}',
						'{$this->getRoom_ref()}'
					)";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao cadastrar');
			}

			$this->setIdFinancial($db->LastInsertId());
		}
	}