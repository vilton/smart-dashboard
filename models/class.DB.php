<?php    
	/**
	 * Classe para manipula��o do banco de dados MySQL
	 * Dados de conex�o devem estar no arquivo infodb.php
	 * constantes necess�rias
	 * AR_DBHOST - host do MySQL
	 * AR_DBUSER - usu�rio do MySQL
	 * AR_DBPASS - senha do MySQL
	 * 
	 * @version 1.5
	 * @author Anderson Ceresa - stratu@gmail.com (2009)
	 * @copyright Reprodu��o autorizada desde que mantido dados do autor.
	 */
	class DB {
		private $conn;
		private $result;

		/**
		 * REABRE a conex�o ao banco de dados
		 *
		 */
		public function __construct(){
		   if(!$this->conn = mysql_connect('mysql.720.nu','720','smartbd')){
				throw new Exception('Erro ao conectar a base de dados');
			}	
			if(!mysql_select_db('720',$this->conn)){
				throw new Exception('Erro ao selecionar a base de dados para uso');
			}
		}
		
		/**
		 * Retorna esta conex�o ao banco de dados
		 *
		 * @return resource
		 */
		public function Conn(){
			return $this->conn;
		}
		
		/**
		 * Fecha a conex�o ao mysql
		 *
		 * @return bool
		 */
		public function Close(){
			return mysql_close($this->conn);
		}
		
		/**
		 * Inicia uma transa��o de banco de dados
		 *
		 */
		public function StartTransaction(){
			if(!mysql_query('START TRANSACTION',$this->Conn())){
				throw new Exception('Erro ao iniciar a transa��o');
			}
		}
		
		/**
		 * Executa o ROLLBACK na transa��o atual
		 *
		 */
		public function Rollback(){
			if(!mysql_query('ROLLBACK',$this->Conn())){
				throw new Exception('Erro ao executar o cancelamento da transa��o');
			}
		}
		
		/**
		 * Executa o COMMIT da transa��o atual
		 *
		 */
		public function Commit(){
			if(!mysql_query('COMMIT',$this->Conn())){
				throw new Exception('Erro ao salvar os dados da transa��o');
			}
		}
		
		/**
		 * Executa um comando SQL no banco de dados
		 *
		 * @param string $query
		 * @return resource
		 */
		public function Sql($query){
			if(!$this->result = mysql_query($query,$this->Conn())){
				return false;
			}else{
				return $this->result;
			}
		}
		
		/**
		 * Retorna uma linha de resultado por chamada
		 *
		 * @return object
		 */
		public function Fetch(){
			return mysql_fetch_object($this->result);
		}
		
		/**
		 * Retorna o n�mero de linhas retornadas pela consulta SQL ou n�mero de linhas modificadas por comandos como UPDATE e DELETE
		 *
		 * @return int
		 */
		public function NumRows(){
			return mysql_num_rows($this->result);
		}
		
		/**
		 * Retorna a mensagem de erro do mysql
		 *
		 * @return string
		 */
		public function Error(){
			return mysql_error($this->Conn());
		}
		
		/**
		 * Retorna o �ltimo autoincrement gerado
		 *
		 * @return int
		 */
		public function LastInsertId(){
			return mysql_insert_id($this->conn);
		}
	}
?>