<?php
	/**
	 * Classe com utilidados para o desenvolvimento
	 * 
	 * 
	 * @author Anderson Ceresa
	 *
	 */
	class Util{
		/**
		 * Exibe uma mensagem via javascript
		 *
		 * @param string $texto
		 */
		public static function Mensagem($texto){
			$texto= utf8_decode(str_replace("'","\'",str_replace(chr(10),'\n',str_replace(chr(13),'',$texto))));
			echo "<script type=\"text/javascript\">
				   alert('$texto');
				 </script>";
		}
		
		/**
		 * Exibe uma mensagem de erro via javascript
		 *
		 * @param string $texto
		 */
		public static function MensagemErro($texto){
			$texto= str_replace("'","\'",str_replace(chr(10),'\n',str_replace(chr(13),'',$texto)));
			echo "<script type=\"text/javascript\">
				   alert('OCORREU UM ERRO NO SISTEMA\\n\\n$texto');
				 </script>";
		}
		
		/**
		 * Redireciona o navegador via javascript
		 *
		 * @param string $url
		 */
		public static function Redireciona($url){
			echo "<script type=\"text/javascript\">
				   document.location.replace('$url');
				  </script>";
		}
		
		/**
		 * Fecha uma janela utilizando o javascrip window.close()
		 *
		 */
		public static function JsWindowClose(){
			echo "<script type=\"text/javascript\">
				   window.close();
				  </script>";
		}
		
		/**
		* Entra no formato DD/MM/AAAA e sai AAAA-MM-DD
		*
		* @param string $data
		* @return string
		*/
		public static function DatePtToDateMysql($data){
			return implode('-',array_reverse(explode('/',$data)));
		}
		
		/**
		* Entra no formato AAAA-MM-DD  e sai DD/MM/AAAA
		*
		* @param string $data
		* @return string
		*/
		public static function DateMysqlToDatePt($data){
			return implode('/',array_reverse(explode('-',$data)));
		}
		
		/**
		 * Testa se uma data ISO(aaaa-mm-dd) é válida
		 *
		 * @param string $data
		 * @return bool
		 */
		public static function VerificaDataISO($data){
			$d= explode('-',$data);
			return checkdate($d[1],$d[2],$d[0]);
		}
		
		/**
		 * Limpa completamente uma string retirando espaços e tags html/php
		 *
		 * @param string $string
		 * @return string
		 */
		public static function LimpaStringCompleta($string){
			return strip_tags(trim($string));
		}
		
		/**
		 * Converte uma string no formato 9.999,99 para double
		 * O parametro casas deve receber o nro de casas decimais retornadas por padrao
		 *
		 * @param string $string
		 * @param int $casas
		 * @return double
		 */
		public static function String2Double($string,$casas= 2){
			return str_replace(',','.',str_replace('.','',$string));
		}
		
		/**
		 * Converte um double para o formato 9.999,99
		 * O parametro casas deve receber o nro de casas decimais retornadas por padrao
		 *
		 * @param double $float
		 * @param int $casas
		 * @return string
		 */
		public static function Double2String($float,$casas= 2){
			return number_format($float,$casas,',','.');
		}
		
		/**
		 * Adiciona zeros a esquerda
		 *
		 * @param int $maxCaracteres
		 * @param string $string
		 * @return string
		 */
		public static function AdiconaZerosEsquerda($maxCaracteres,$string){
			$auxStr = '';
			$tamanho= strlen($string);
			$auxCont= $maxCaracteres - $tamanho;
			if($auxCont > 0){
				for($x=1;$x<=$auxCont;$x++){
					$auxStr.= '0';
				}
			}
			
			return $auxStr.$string;
		}
		
		/**
		 * Verifica se o email é válido (formato)
		 *
		 * @param string $email
		 * @return bool
		 */
		public static function VerificaEmail($email){
			if(eregi("^([a-z0-9_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,4}$",$email)){
				return true;
			}else{
				return false;
			}
		}
		
		/**
		 * Verifica se a url é valida (formato)
		 *
		 * @param string $url
		 * @return bool
		 */
		public static function VerificaURL($url){
			if(ereg('^(http|https|ftp)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&%\$#\=~])*$',$url) != false){
				return true;
			}else{
				return false;
			}
		}
		
		/**
		 * Verifica se o telefone possui 9 ou 10 digitos numéricos
		 *
		 * @param string $telefone
		 * @return bool
		 */
		public static function VerificaTelefone($telefone){
			if(ereg('[0-9]{9,10}',$telefone) != false){
				return true;
			}else{
				return false;
			}
		}
		
		/**
		 * Verifica o telefone com a mascara (99)9999-9999 ou (99)999-9999
		 *
		 * @param string $telefone
		 * @return bool
		 */
		public static function VerificaTelefoneMascara($telefone){
			if(ereg('\([0-9]{2}\)[0-9]{4}-[0-9]{4}',$telefone) != false){
				return true;
			}else{
				return false;
			}
		}
		
		/**
		 * Valida o formato do CPF.
		 * Não identifica se o dígito verificador é válido
		 *
		 * @param string $cpf
		 * @return bool
		 */
		public static function VerificaCPF($cpf){
			if(ereg('[0-9]{3}.[0-9]{3}.[0-9]{3}-[0-9]{2}',$cpf) != false){
				return true;
			}else{
				return false;
			}
		}
		
		/**
		 * Valida o formato do CEP.
		 *
		 * @param string $cep
		 * @return bool
		 */
		public static function VerificaCEP($cep){
			if(ereg('[0-9]{5}-[0-9]{3}',$cep) != false){
				return true;
			}else{
				return false;
			}
		}
		
		/**
		 * Monta um campo de data com calendário
		 *
		 * @param string $nomeInput nome do input
		 * @param string $idInput id do input
		 * @param int $widthInput larguara do input em pixels
		 * @param string $classInput classe css do input
		 * @param bool $inputReadOnly se é somente leitura ou nao
		 * @param string $valueInput value do input
		 * @param string $idBotao id do botao
		 * @param string $valueBotao value do botao - normalmente em branco
		 * @param string $classBotao classe css do botao
		 * @param string $formatoData fotmato de data
		 * @param bool $singleClick se em um unico clic seleciona a data - padrao true
		 * @param bool $disableButton se desabilita o botao - padrao false
		 * 
		 * @return string
		 */
		public static function MontaCampoData($nomeInput,$idInput,$classInput,$inputReadOnly,$valueInput,$idBotao,$valueBotao,$classBotao,$formatoData,$singleClick,$disableButton= false){
			$textoInputReadOnly= $inputReadOnly == true ? 'readonly' : null;
			$desabilitaBotao   = $disableButton == true ? 'disabled' : null;
			$textoSingleClick  = $singleClick == true ? 'true' : 'false';
			return "<input type=\"text\" name=\"{$nomeInput}\" {$textoInputReadOnly} style=\"width: 60px;\" maxlength=\"10\" value=\"{$valueInput}\" id=\"{$idInput}\" class=\"{$classInput}\">
					<input type=\"button\" {$desabilitaBotao} id=\"{$idBotao}\" value=\"{$valueBotao}\" class=\"$classBotao\">
					<script type=\"text/javascript\">
						Calendar.setup({
							inputField     :    \"$idInput\",      // id of the input field
							ifFormat       :    \"$formatoData\",       // format of the input field
							button         :    \"$idBotao\",   // trigger for the calendar (button ID)
							singleClick    :    $textoSingleClick            // double-click mode
						});
					</script>";
			
		}
		
		/**
		 * Monta os campos para hora
		 *
		 * @param string $classInput
		 * @param string $nomeHora
		 * @param string $idHora
		 * @param string $valueHora
		 * @param string $nomeMinuto
		 * @param string $idMinuto
		 * @param string $valueMinuto
		 * @param string $nomeSegundo
		 * @param string $idSegundo
		 * @param string $valueSegundo
		 * 
		 * @return string
		 */
		public static function MontaCampoHora($classInput,$nomeHora,$idHora,$valueHora,$nomeMinuto= '',$idMinuto= '',$valueMinuto= '',$nomeSegundo= '',$idSegundo= '',$valueSegundo= ''){
			$x= "<input type=\"text\" name=\"{$nomeHora}\" id=\"{$idHora}\" style=\"width: 20px;text-align: center;\" maxlength=\"2\" value=\"{$valueHora}\" class=\"{$classInput}\">";
			if(!empty($nomeMinuto)){
				$x.= " : <input type=\"text\" name=\"{$nomeMinuto}\" id=\"{$idMinuto}\" style=\"width: 20px;text-align: center;\" maxlength=\"2\" value=\"{$valueMinuto}\" class=\"{$classInput}\">";
			}
			if(!empty($nomeSegundo)){
				$x.= " : <input type=\"text\" name=\"{$nomeSegundo}\" id=\"{$idSegundo}\" style=\"width: 20px;text-align: center;\" maxlength=\"2\" value=\"{$valueSegundo}\" class=\"{$classInput}\">";
			}
			return $x;
		}
		
		/**
		 * Gera uma senha aleatória simples
		 * O parametro $caixa deve receber o tipo de caixa que sera mostrado
		 * BAIXA - Retorna a senha em caixa baixa
		 * ALTA - Retorna a senha em caixa ALTA
		 * IGNORA - Retorna a senha sem mudança de caixa
		 * 
		 * @param int $maxCaracteres
		 * @param string $caixa
		 * @return string
		 */
		public static function GeraSenhaAleatoria($maxCaracteres,$caixa= 'IGNORA'){
			$lista= 'abcdefghijklmnopqrstuvxywzABCDEFGHIJKLMNOPQRSTUVXYWZ0123456789';
			$max= strlen($lista) - 1;
			$senha= '';
			for($x= 1;$x <= $maxCaracteres;$x++){
				$senha.= $lista{mt_rand(0,$max)};
			}
			switch($caixa){
				case 'IGNORA': return $senha; break;
				case 'BAIXA': return strtolower($senha); break;
				case 'ALTA': return strtoupper($senha); break;
			}
		}
		
		/**
		 * Monta a variável $_SERVER['DOCUMENT_ROOT'] no IIS
		 * Essa superglobal não (??!!) está disponível no IIS (???!!!)
		 *
		 */
		public static function MontaServerDocumentRootIIS(){
			 if(!isset($_SERVER['DOCUMENT_ROOT'])){ 
				 if(isset($_SERVER['SCRIPT_FILENAME'])){
					 $_SERVER['DOCUMENT_ROOT'] = str_replace( '\\', '/', substr($_SERVER['SCRIPT_FILENAME'], 0, 0-strlen($_SERVER['PHP_SELF'])));
				 }
				 if(isset($_SERVER['PATH_TRANSLATED'])){
					$_SERVER['DOCUMENT_ROOT'] = str_replace( '\\', '/', substr(str_replace('\\\\', '\\', $_SERVER['PATH_TRANSLATED']), 0, 0-strlen($_SERVER['PHP_SELF'])));
				 }
			 }
		}
		
		/**
		 * Monta a lista de options de uma combobox html com base em uma consulta a banco de dados
		 * Retorna a lista de options ou um options com a mensagem ERRO em caso de erro na consulta SQL
		 *
		 * @param string $query Comando SQL que será executado
		 * @param string $campoValue Campo cujo valor será colocado no atributo value da option
		 * @param string $campoOpcao Campo cujo valor será mostrado na option
		 * @param string $campoComparacao Campo cujo valor será utilizado para decidir se o option é pré-selecionado ou não
		 * @param string $valorComparacao Valor que será utilizado para decidir se o option é pré-selecionado ou não
		 * @return string
		 */
		public static function MontaOptionsSelect($query,$campoValue,$campoOpcao,$campoComparacao,$valorComparacao){
			$db= new DB();
			$retorna= '';
			if(!$result= mysql_query($query,$db->Conn())){
				$retorna= '<option>ERRO</option>';
			}else{
				while($dado= mysql_fetch_array($result)){
					$seleciona= $dado[$campoComparacao] == $valorComparacao ? 'selected="selected"' : null;
					$retorna.= "<option value=\"{$dado[$campoValue]}\" {$seleciona}>{$dado[$campoOpcao]}</option>\n";
				}
			}
			return $retorna;
		}
		
		/**
		 * Gera um token unico simples com mt_rand e sha1
		 *
		 * @return unknown
		 */
		public static function GeraToken(){
			return strtoupper(sha1(mt_rand()));
		}
		
		/**
		 * Retira SOMENTE os separadores de casas decimais e de milhar de um número double.
		 * Devolve como um número inteiro
		 *
		 * @param double $numero
		 * @return int
		 */
		public static function RetiraSeparadoresNumero($numero){
			return (int) str_replace('.','',str_replace(',','',$numero));
		}
		
		public function formataData($date){
			$dataFinal = implode('-', array_reverse(explode('/',$date)));
			return $dataFinal;
		}
		public function verData($date){
			$dataFinal = implode('/', array_reverse(explode('-',$date)));
			return $dataFinal;
		}
	}

?>