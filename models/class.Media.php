<?php
	class User {
		private $iduser;
		private $cpf;
		private $email;

		public function __Construct(){
			$this->iduser= 0;
			$this->cpf= '';
			$this->email= '';
		}

		public function getIduser() {
			return $this->iduser;
		}
		public function setIduser($iduser) {
			return $this->iduser = (int) $iduser;
		}
		public function getCpf() {
			return $this->cpf;
		}
		public function setCpf($cpf) {
			return $this->cpf = $cpf;
		}
		public function getEmail() {
			return $this->email;
		}
		public function setEmail($email) {
			return $this->email = $email;
		}

		public function Carrega($id){
			$id= (int) $id;

			$query= "SELECT * 
					 FROM User
					 WHERE iduser = {$id}";
			$db= new DB();
			$db->Sql($query);

			if($db->NumRows() == 0){
				throw new Exception('Invalido');
			}

			$dado= $db->Fetch();

			$this->setIduser($dado->iduser);
			$this->setCpf($dado->cpf);
			$this->setCpf($dado->email);
		}

		public function Cadastra(){
			$db= new DB();
			$query = "INSERT INTO User (
						cpf,
						email
					)VALUES(
						'{$this->getCpf()}',
						'{$this->getEmail()}'
					)";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao cadastrar');
			}

			$this->setIduser($db->LastInsertId());
		}

		public function CadastraFotos(){
			$id= $this->getIdimovel();
			$db= new DB();
			
			include('../includes/m2brimagem.class.php');
			//$diretorio = 'images/';
			//$dir_thumbs = $diretorio.'thumbs/';
			$diretorio = '../gerenciador/images/' . DIRECTORY_SEPARATOR;

			$arquivo = isset($_FILES['fotos']) ? $_FILES['fotos'] : FALSE;

			for ($i = 0; $i < count($arquivo['name']); $i++) {

				$imagem_nome =   md5(uniqid(time())) . $arquivo['name'][$i];
				$palavra = strtolower($imagem_nome);
				$palavra = ereg_replace("[^a-zA-Z0-9_.]", "", strtr($palavra, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"));

				// if(eregi(".+.[jJ][pP][eE]?[gG]$", $palavra)) {
					if (move_uploaded_file($arquivo['tmp_name'][$i], $diretorio . $palavra)) {
						$oImg = new m2brimagem();
						$oImg->carrega($diretorio.$palavra);
						$valida = $oImg->valida();
						if ($valida == 'OK') {
							$largura = "800";
							$altura = "";

							$oImg->redimensiona($largura,$altura,'crop');
							$tamanho = getimagesize($diretorio.$palavra);

							if($tamanho[0] > 800) {
								$oImg->grava($diretorio.$palavra,90);
							}

							//echo "Miniatura criada para " . $diretorio.$palavra . "<hr />";
							$query= "INSERT INTO fotos (
									idimovel,
									caminho,
									externo
									)VALUES(
									'{$id}',
									'{$palavra}',
									'0'
									)";
							if(!$db->Sql($query)){
								throw new Exception('Falha ao cadastrar a foto');
							}
							$this->setIdfoto($db->LastInsertId());
						}
						else {
							echo "Erro ao criar miniatura para " . $diretorio.$palavra . ": " . $valida . "<hr />";
						}
					} else {
						echo "erro";
					}
				// }
			}
		}

		public function Edita(){
			$db= new DB();
			$query = "UPDATE User SET
						cpf= '{$this->getCpf()}',
						email= '{$this->getEmail()}'
					WHERE iduser = {$this->getIduser()}";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao editar');
			}
		}

		public function Remove(){
			$db= new DB();
			$query = "DELETE FROM User 
						WHERE iduser = {$this->getIduser()}
						LIMIT 1";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao remover');
			}
		}
	}