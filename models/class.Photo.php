<?php
	include('m2brimagem.class.php');

	class Photo {
		private $idphoto;
		private $title;
		private $description;
		private $date;
		private $type;
		private $Build_idbuild;
		private $file;
		private $filetemp;

		public function __Construct(){
			$this->idphoto= 0;
			$this->title= '';
			$this->description= '';
			$this->date= 0;
			$this->type= '';
			$this->Build_idbuild= '';
			$this->file= '';
			$this->filetemp= '';
		}

		public function getIdphoto() {
			return $this->idphoto;
		}
		public function setIdphoto($idphoto) {
			return $this->idphoto = (int) $idphoto;
		}
		public function getTitle() {
			return $this->title;
		}
		public function setTitle($title) {
			return $this->title = $title;
		}
		public function getDescription() {
			return $this->description;
		}
		public function setDescription($description) {
			return $this->description = $description;
		}
		public function getDate() {
			return $this->date;
		}
		public function setDate($date) {
			return $this->date = $date;
		}
		public function getType() {
			return $this->type;
		}
		public function setType($type) {
			return $this->type = $type;
		}
		public function getBuild_idbuild() {
			return $this->Build_idbuild;
		}
		public function setBuild_idbuild($Build_idbuild) {
			return $this->Build_idbuild = $Build_idbuild;
		}

		public function getFile() {
			return $this->file;
		}
		public function setFile($file) {
			return $this->file = $file;
		}
		public function getFileTemp() {
			return $this->filetemp;
		}
		public function setFileTemp($filetemp) {
			return $this->filetemp = $filetemp;
		}

		public function Carrega($id){
			$id= (int) $id;

			$query= "SELECT * 
					 FROM Photo
					 WHERE idphoto = {$id}";
			$db= new DB();
			$db->Sql($query);

			if($db->NumRows() == 0){
				throw new Exception('Invalido');
			}

			$dado= $db->Fetch();

			$this->setIdphoto($dado->idphoto);
			$this->setTitle($dado->title);
			$this->setDescription($dado->description);
			$this->setDate($dado->date);
			$this->setType($dado->type);
			$this->setFile($dado->pathway);
			$this->setBuild_idbuild($dado->Build_idbuild);
		}

		public function loadByDate($date){
			$date = $date;

			$query= "SELECT * 
					 FROM Photo
					 WHERE date = '{$date}'";
			$db= new DB();
			$db->Sql($query);

			if($db->NumRows() == 0){
				throw new Exception('Invalido');
			}

			$dado= $db->Fetch();

			$this->setIdphoto($dado->idphoto);
			$this->setTitle($dado->title);
			$this->setDescription($dado->description);
			$this->setDate($dado->date);
			$this->setType($dado->type);
			$this->setFile($dado->pathway);
			$this->setBuild_idbuild($dado->Build_idbuild);
		}

		public function loadLast($id){
			$id= (int) $id;

			$query= "SELECT 
						* 
					 FROM 
					 	Photo
					 WHERE
					 	Build_idbuild = {$id}
					 ORDER BY
					 	date
					 DESC
					 LIMIT
					 	1";
			$db= new DB();
			$db->Sql($query);

			if($db->NumRows() == 0){
				throw new Exception('Invalido');
			}

			$dado= $db->Fetch();

			$this->setIdphoto($dado->idphoto);
			$this->setTitle($dado->title);
			$this->setDescription($dado->description);
			$this->setDate($dado->date);
			$this->setType($dado->type);
			$this->setFile($dado->pathway);
			$this->setBuild_idbuild($dado->Build_idbuild);
		}

		public function savePhoto(){
			$db= new DB();

			$query = "INSERT INTO 
						Photo (
							pathway,
							title,
							description,
							type,
							date,
							Build_idbuild
						) VALUES 
						(
							'{$this->getFile()}',
							'{$this->getTitle()}',
							'{$this->getDescription()}',
							'{$this->getType()}',
							'{$this->getDate()}',
							'{$this->getBuild_idbuild()}'
						)";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao cadastrar a foto');
			}
			$this->setIdphoto($db->LastInsertId());
		}

		public function Edita(){
			$db= new DB();
			$query = "UPDATE Photo SET
						title= '{$this->getTitle()}',
						description= '{$this->getDescription()}'
					WHERE idphoto = {$this->getIdphoto()}";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao editar');
			}
		}

		public function Remove(){
			$db= new DB();
			$query = "DELETE FROM Photo 
						WHERE idphoto = {$this->getIdphoto()}
						LIMIT 1";
			if(!$db->Sql($query)){
				throw new Exception('Falha ao remover');
			}
		}
		public function CadastraFoto(){
			$db= new DB();

			$diretorio = '../images' . DIRECTORY_SEPARATOR;

			$imagem_nome =   md5(uniqid(time())) . $this->getFile();
			$palavra = strtolower($imagem_nome);
			$palavra = ereg_replace("[^a-zA-Z0-9_.]", "", strtr($palavra, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"));

			if (move_uploaded_file($this->getFileTemp(), $diretorio . $palavra)) {
				$oImg = new m2brimagem();
				$oImg->carrega($diretorio.$palavra);
				$valida = $oImg->valida();
				if ($valida == 'OK') {
					$largura = "800";
					$altura = "";

					$oImg->redimensiona($largura,$altura,'crop');
					$tamanho = getimagesize($diretorio.$palavra);

					if($tamanho[0] > 800) {
						$oImg->grava($diretorio.$palavra,90);
					}

					$this->setFile($palavra);

					$this->savePhoto();
				}
				else {
					throw new Exception("Erro ao criar miniatura para " . $diretorio.$palavra . ": " . $valida);
				}
			} else {
				throw new Exception("Erro");
			}
		}
	}